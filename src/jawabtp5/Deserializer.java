/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jawabtp5;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juvetic
 */
public class Deserializer {
    
    public static void main(String[] args) {
        Deserializer d = new Deserializer();
        Person p = d.deserializePerson();
        System.out.println(p.toString());
    }
    
    public Person deserializePerson() {
        
        Person p;
        
        try {
            FileInputStream fi = new FileInputStream("D:\\person.ser");
            ObjectInputStream is = new ObjectInputStream(fi);
            p = (Person) is.readObject();
            is.close();
            return p;
        } catch (Exception ex) {
            System.out.println("File gagal dibaca!");
            return null;
        }
    }
}
