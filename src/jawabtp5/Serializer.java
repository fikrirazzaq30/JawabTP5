/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jawabtp5;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juvetic
 */
public class Serializer {
    
    public static void main(String[] args) {
        Serializer s = new Serializer();
        s.serializePerson("Dybala", "Sukamenak Indah", 20);
    }
    
    public void serializePerson(String name, String address, int age) {
        
        //Instansiasi
        Person p = new Person(name, address, age);
        
        try {
            //Simpan objek ke File
            FileOutputStream fo = new FileOutputStream("D:\\person.ser");
            ObjectOutputStream os = new ObjectOutputStream(fo);
            os.writeObject(p);
            os.close();
            System.out.println("File berhasil disimpan!");
        } catch (Exception ex) {
            System.out.println("File gagal disimpan!");
        }
    }
}
